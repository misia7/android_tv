package swim.tv

import android.app.Activity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bieszczady = Image(R.drawable.bieszczady, "Bieszczady")
        val fjords = Image(R.drawable.fjords, "Fjords")
        val laguna = Image(R.drawable.laguna, "Laguna")

        var imagesList = mutableListOf<Image>()

        imagesList.add(bieszczady)
        imagesList.add(fjords)
        imagesList.add(laguna)



        image2.setOnClickListener{
            show_image.setImageResource(imagesList[1].img)
            title_text.text = imagesList[1].title
        }

        image3.setOnClickListener{
            show_image.setImageResource(imagesList[2].img)
            title_text.text = imagesList[2].title
        }

        image1.setOnClickListener{
            show_image.setImageResource(imagesList[0].img)
            title_text.text = imagesList[0].title
        }
    }
}
